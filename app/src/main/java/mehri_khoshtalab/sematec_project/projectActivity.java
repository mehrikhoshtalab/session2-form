package mehri_khoshtalab.sematec_project;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class projectActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project1);


        final EditText enterName = (EditText) findViewById(R.id.enterName);
        final EditText enterFamily = (EditText) findViewById(R.id.enterFamily);
        final TextView detail = (TextView) findViewById(R.id.detail);
        Button showDetail = (Button) findViewById(R.id.showDetail);
        showDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                detail.setText(enterName.getText().toString() + " " + enterFamily.getText().toString());
            }
        });


    }
}
